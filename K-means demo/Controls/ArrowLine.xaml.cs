﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace K_means_demo.Controls
{
    /// <summary>
    /// Interaction logic for ArrowLine.xaml
    /// </summary>
    public partial class ArrowLine : UserControl
    {


        public static double GetX1(DependencyObject obj)
        {
            return (double)obj.GetValue(X1Property);
        }

        public static void SetX1(DependencyObject obj, double value)
        {
            obj.SetValue(X1Property, value);
        }

        // Using a DependencyProperty as the backing store for X1.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty X1Property =
            DependencyProperty.RegisterAttached("X1", typeof(double), typeof(ArrowLine), new PropertyMetadata(0.0, positionChangedCallback));
        
        public static double GetY1(DependencyObject obj)
        {
            return (double)obj.GetValue(Y1Property);
        }

        public static void SetY1(DependencyObject obj, double value)
        {
            obj.SetValue(Y1Property, value);
        }

        // Using a DependencyProperty as the backing store for Y1.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Y1Property =
            DependencyProperty.RegisterAttached("Y1", typeof(double), typeof(ArrowLine), new PropertyMetadata(0.0, positionChangedCallback));



        public static double GetX2(DependencyObject obj)
        {
            return (double)obj.GetValue(X2Property);
        }

        public static void SetX2(DependencyObject obj, double value)
        {
            obj.SetValue(X2Property, value);
        }

        // Using a DependencyProperty as the backing store for X2.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty X2Property =
            DependencyProperty.RegisterAttached("X2", typeof(double), typeof(ArrowLine), new PropertyMetadata(0.0, positionChangedCallback));
        

        public static double GetY2(DependencyObject obj)
        {
            return (double)obj.GetValue(Y2Property);
        }

        public static void SetY2(DependencyObject obj, double value)
        {
            obj.SetValue(Y2Property, value);
        }

        // Using a DependencyProperty as the backing store for Y2.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Y2Property =
            DependencyProperty.RegisterAttached("Y2", typeof(double), typeof(ArrowLine), new PropertyMetadata(0.0, positionChangedCallback));

        
        public static double GetLength(DependencyObject obj)
        {
            return (double)obj.GetValue(LengthProperty);
        }

        public static void SetLength(DependencyObject obj, double value)
        {
            obj.SetValue(LengthProperty, value);
        }

        // Using a DependencyProperty as the backing store for Length.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LengthProperty =
            DependencyProperty.RegisterAttached("Length", typeof(double), typeof(ArrowLine), new PropertyMetadata(0.0));

        private static void positionChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ArrowLine a = d as ArrowLine;
            if(a != null) a.CalculateLenth();
        }

        public void CalculateLenth()
        {
            double x = GetX2(this) - GetX1(this);
            double y = GetY2(this) - GetY1(this);
            SetLength(this, Math.Sqrt(x * x + y * y));
        }



        public ArrowLine()
        {
            InitializeComponent();
        }
    }
}
