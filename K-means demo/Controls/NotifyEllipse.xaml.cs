﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace K_means_demo.Controls
{
    /// <summary>
    /// Interaction logic for NotifyEllipse.xaml
    /// </summary>
    public partial class NotifyEllipse : UserControl
    {


        public Thickness StrokeThickness
        {
            get { return (Thickness)GetValue(StrokeThicknessProperty); }
            set { SetValue(StrokeThicknessProperty, value); }
        }
        public static readonly DependencyProperty StrokeThicknessProperty =
            DependencyProperty.Register("StrokeThickness", typeof(Thickness), typeof(NotifyEllipse), new PropertyMetadata(new Thickness(1)));
        

        public Brush Fill
        {
            get { return (Brush)GetValue(FillProperty); }
            set { SetValue(FillProperty, value); }
        }

        public static readonly DependencyProperty FillProperty =
            DependencyProperty.Register("Fill", typeof(Brush), typeof(NotifyEllipse), new PropertyMetadata(Brushes.Black, new PropertyChangedCallback(OnFillChanged)));



        public static readonly RoutedEvent FillChangedEvent =
       EventManager.RegisterRoutedEvent("FillChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(NotifyEllipse));

        public event RoutedEventHandler FillChanged
        {
            add { this.AddHandler(FillChangedEvent, value); }
            remove { this.RemoveHandler(FillChangedEvent, value); }
        }

        private static void OnFillChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            NotifyEllipse ellipse = d as NotifyEllipse;
            //ellipse.EllipseCntrl.Stroke = ellipse.EllipseCntrl.Fill;
            ellipse.EllipseCntrl.Fill = e.NewValue as Brush;
            ellipse.RaiseEvent(new RoutedEventArgs(FillChangedEvent));
        }

        public NotifyEllipse()
        {
            InitializeComponent();
        }
    }
}
