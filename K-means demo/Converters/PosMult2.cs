﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace K_means_demo.Model
{
    public class CanvasPositionMultiValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                double data_pos = (double)values[0];
                Range data_range = (Range)values[1];
                double canvas_size = (double)values[2];
                double scale = canvas_size / (data_range.To - data_range.From);
                return (data_pos - data_range.From) * scale;
            }
            catch (Exception e)
            {
                throw new ArgumentException("Unexpected converter input", e);
            }

        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class CanvasPointMultiValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                PointCollection data_pos = (PointCollection)values[0];
                Range data_rangeX = (Range)values[1];
                Range data_rangeY = (Range)values[2];
                double canvas_w = (double)values[3];
                double canvas_h = (double)values[4];
                double scalew = canvas_w / (data_rangeX.To - data_rangeX.From);
                double scaleh = canvas_h / (data_rangeY.To - data_rangeY.From);
                PointCollection res = new PointCollection(data_pos.Select(p => new Point((p.X - data_rangeX.From) * scalew, (p.Y - data_rangeY.From) * scaleh)));
                return res;
            }
            catch (Exception e)
            {
                throw new ArgumentException("Unexpected converter input", e);
            }

        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
