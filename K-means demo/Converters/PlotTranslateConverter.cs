﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace K_means_demo.Model
{
    class PlotTranslateConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length != 3) return DependencyProperty.UnsetValue;
            try
            {
                var val = Array.ConvertAll(values, o => (double)o);

                return val[0] - (val[1] * 0.9);/*scale all values so that the sortest bar is 10% of lowest value*/

            }
            catch (Exception e)
            {
                throw new ArgumentException("Unexpected converter input", e);
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
