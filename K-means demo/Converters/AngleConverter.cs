﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace K_means_demo.Model
{
    public class AngleConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length != 4) return DependencyProperty.UnsetValue;
            try
            {
                var points = Array.ConvertAll(values, o => (double)o);
                double x = points[0] - points[2];
                double y = points[1] - points[3];

                return 270 - (Math.Atan2(x, y) / Math.PI * 180.0);

            }
            catch(Exception e)
            {
                throw new ArgumentException("Unexpected converter input", e);
            }

        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
