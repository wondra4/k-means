﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K_means_demo.Utils
{
    public static class LinqExtensions
    {
        public static T MinBy<T>(this IEnumerable<T> list, Func<T, T, bool> greaterThan)
        {
            return list.Aggregate((first, second) => (first == null || greaterThan(first, second) ? second : first));
        }
        
    }
}
