﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using K_means_demo.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Voronoi2;

namespace K_means_demo.Model
{
    public enum InteractionCommandMode
    {
        info, addVertex, moveVertex
    }

    public class DataSet : ViewModelBase
    {
        private ObservableCollection<DataPoint> _data;
        public ObservableCollection<DataPoint> Data
        {
            get { return _data; }
            set { _data = value; }
        }

        private ObservableCollection<Cluster> _clusters;
        public ObservableCollection<Cluster> Clusters
        {
            get { return _clusters; }
            set { _clusters = value; }
        }
        
        private ObservableCollection<VoronoiCell> _voCells;

        public ObservableCollection<VoronoiCell> VoronoiCells
        {
            get { return _voCells; }
            set { _voCells = value; }
        }

        public ObservableCollection<double> ErrorHistory { get; private set; }
        public double ErrorMin { get { return ErrorHistory.Min(); } }
        public double ErrorMax { get { return ErrorHistory.Max(); } }

        private Cluster _nullCluster;

        public RelayCommand<string> GenerateDataCommand { get; private set; }

        public RelayCommand<object> CanvasClickCommand { get; private set; }

        public RelayCommand ClearDataCommand { get; private set; }
        public RelayCommand FindClustersCommand { get; private set; }
        public RelayCommand MoveCentroidsCommand { get; private set; }
        public RelayCommand NextStepCommand { get; private set; }
        public RelayCommand ResetCommand { get; private set; }
        public RelayCommand RestartCommand { get; private set; }

        private Range _xrange;
        public Range RangeX
        {
            get { return _xrange; }
            set
            {
                if (value != _xrange)
                {
                    _xrange = value;
                    RaisePropertyChanged();
                }
            }
        }

        private int _step;
        public int Step
        {
            get { return _step; }
            set
            {
                if (value != _step)
                {
                    _step = value;
                    RaisePropertyChanged();
                }
            }
        }
        
        private double _canvasW;
        public double CanvasW
        {
            get { return _canvasW; }
            set { _canvasW = value; }
        }

        private double _canvasH;
        public double CanvasH
        {
            get { return _canvasH; }
            set { _canvasH = value; }
        }

        private string _clusterCount = "3";
        public string ClusterCount
        {
            get { return _clusterCount; }
            set { _clusterCount = value; }
        }

        private double _dW;
        public double DeltaWeight
        {
            get { return _dW; }
            set
            {
                if (value != _dW)
                {
                    _dW = value;
                    RaisePropertyChanged();
                }
            }
        }

        private double _weight;
        public double Weight
        {
            get { return _weight; }
            set
            {
                if (value != _weight)
                {
                    _weight = value;
                    RaisePropertyChanged();
                }
            }
        }

        private Range _yrange;
        public Range RangeY
        {
            get { return _yrange; }
            set
            {
                if (value != _yrange)
                {
                    _yrange = value;
                    RaisePropertyChanged();
                }
            }
        }

        private InteractionCommandMode _interactionMode;
        public InteractionCommandMode InteractionMode
        {
            get { return _interactionMode; }
            set
            {
                if (value != _interactionMode)
                {
                    _interactionMode = value;
                    RaisePropertyChanged();
                }
            }
        }

        public DataSet()
        {
            _clusters = new ObservableCollection<Cluster>();
            _data = new ObservableCollection<DataPoint>();
            _voCells = new ObservableCollection<VoronoiCell>();
            ErrorHistory = new ObservableCollection<double>();
            _xrange = new Range() { From = 0.0, To = 1.0 };
            _yrange = new Range() { From = 0.0, To = 1.0 };
            _dW = double.PositiveInfinity;
            _weight = double.PositiveInfinity;

            GenerateDataCommand = new RelayCommand<string>(GenerateData);
            FindClustersCommand = new RelayCommand(FindClusters);
            MoveCentroidsCommand = new RelayCommand(MoveCentroids);
            ClearDataCommand = new RelayCommand(() => _data.Clear());
            CanvasClickCommand = new RelayCommand<object>(canvasClick);
            NextStepCommand = new RelayCommand(nextStep);
            ResetCommand = new RelayCommand(resetAll);
            RestartCommand = new RelayCommand(restart);

            _nullCluster = new Cluster() { Brush = Brushes.Gray, Centroid = new DataPoint() { X = 0.5, Y = 0.5 } }; //null cluster
        }

        private void restart()
        {
            Step = 0;
            anonymizePoints();
            _clusters.Clear();
            _voCells.Clear();
            DeltaWeight = double.PositiveInfinity;
            Weight = double.PositiveInfinity;
            ErrorHistory.Clear();
        }

        private void resetAll()
        {
            Step = 0;
            _data.Clear();
            _clusters.Clear();
            _voCells.Clear();
            DeltaWeight = double.PositiveInfinity;
            Weight = double.PositiveInfinity;
            ErrorHistory.Clear();
        }

        private void refreshWeight()
        {
            double old_w = _weight;
            Weight = _data.Sum(p => p.EuclideanNormSq(p.Cluster.Centroid));
            DeltaWeight = old_w - _weight;
            ErrorHistory.Add(Weight);
            RaisePropertyChanged("ErrorMin");
            RaisePropertyChanged("ErrorMax");
        }

        private void nextStep()
        {
            if(_step < 4) Step++;
            if (_step == 1) InitializeClusters();
            if (_step == 2) FindClusters();
            if (_step == 3) MoveCentroids();
            if (_step == 4 && _dW != 0) Step = 1;
        }
        
        private void canvasClick(object obj)
        {
            if(_interactionMode == InteractionCommandMode.addVertex)
            {
                System.Windows.Point mousePos = Mouse.GetPosition((IInputElement)obj);
                var pos = new System.Windows.Point((mousePos.X / CanvasW) * (RangeX.To - RangeX.From) + RangeX.From, (mousePos.Y / CanvasH) * (RangeY.To - RangeY.From) + RangeY.From);
                if (InteractionMode == InteractionCommandMode.addVertex)
                {
                    _data.Add(new DataPoint() { Cluster = _nullCluster, X = pos.X, Y = pos.Y });
                }
            }
        }

        private void MoveCentroids()
        {
            var clstrs = _data.GroupBy(p => p.Cluster);
            var w = _weight;

            foreach(var g in clstrs)
            {
                var first = g.FirstOrDefault();
                if(first != null)
                {
                    var x = g.Average(p => p.X);
                    var y = g.Average(p => p.Y);
                    first.Cluster.Centroid = new DataPoint() { X = x, Y = y, Cluster = first.Cluster };
                }
            }

            refreshWeight();
            refreshVoronoi();
        }

        private void FindClusters()
        {
            var w = _weight;
            foreach (var p in _data)
            {
                p.Cluster = _clusters.MinBy( (first, second) => { return first.Centroid.EuclideanNormSq(p) > second.Centroid.EuclideanNormSq(p); });
            }
            refreshWeight();
        }

        private void GenerateData(string number)
        {
            int count = 0;
            int.TryParse(number, out count);
            //_data.Clear();
            _xrange = new Range() { From = -0.05, To = 1.05 };
            _yrange = new Range() { From = -0.05, To = 1.05 };

            int size = 1;
            var rnd = new Random();

            for (int i = 0; i < count; i++)
            {
                _data.Add(new DataPoint() { X = rnd.NextDouble() * size, Y = rnd.NextDouble() * size, Cluster = _nullCluster });
            }
        }

        private void anonymizePoints()
        {
            foreach (var p in _data) p.Cluster = _nullCluster;
        }

        private void refreshVoronoi()
        {
            double[] xVals = _clusters.Select(c => c.Centroid.X).ToArray();
            double[] yVals = _clusters.Select(c => c.Centroid.Y).ToArray();
            foreach (var c in _voCells) c.Edges.Clear();

            // Generate the diagram
            Voronoi voroObject = new Voronoi(0.000001);
            List<GraphEdge> ge = voroObject.generateVoronoi(xVals, yVals, RangeX.From, RangeX.To, RangeY.From, RangeY.To);
            
            foreach (var e in ge)
            {
                if (e.x1 != e.x2 || e.y1 != e.y2) //remove degenerate edges
                {
                    _voCells[e.site1].Edges.Add(new CellEdge() { X1 = e.x1, X2 = e.x2, Y1 = e.y1, Y2 = e.y2 });
                    _voCells[e.site2].Edges.Add(new CellEdge() { X1 = e.x2, X2 = e.x1, Y1 = e.y2, Y2 = e.y1 });
                }
            }

            foreach (var c in _voCells) c.MakePolygon(RangeX, RangeY);
        }

        private void InitializeClusters()
        {
            int count = 0;
            int.TryParse(ClusterCount, out count);
            if (count < 2) count = 2;

            _clusters.Clear();
            _voCells.Clear();

            var rnd = new Random();
            int dataCount = _data.Count;
            int usedCount = 0;
            bool[] used = new bool[dataCount];

            for (int i = 0; i < count; i++)
            {
                byte r, g, b;
                ColorUtil.HsvToRgb(i * (360.0 / count), 0.8, 0.75, out r, out g, out b);
                Cluster c = null;
                if (usedCount < dataCount)
                {
                    int point = rnd.Next(dataCount);
                    while (used[point]) point++;
                    used[point] = true;
                    c = new Cluster() { Brush = new SolidColorBrush(Color.FromRgb(r, g, b)), Centroid = new DataPoint() { X = _data[point].X, Y = _data[point].Y } };
                }
                else
                    c = new Cluster() { Brush = new SolidColorBrush(Color.FromRgb(r, g, b)), Centroid = new DataPoint() { X = rnd.NextDouble(), Y = rnd.NextDouble() } };
                _clusters.Add(c);
                _voCells.Add(new VoronoiCell() { Cluster = c });
            }

            anonymizePoints();
            //refreshWeight();
            //DeltaWeight = _weight;
            refreshVoronoi();
        }
        
    }
}
