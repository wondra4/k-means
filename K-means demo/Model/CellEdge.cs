﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K_means_demo.Model
{
    public class CellEdge
    {
        public double X1 { get; set; }
        public double Y1 { get; set; }

        public double X2 { get; set; }
        public double Y2 { get; set; }

        
        public bool isLeft(double pX, double pY)
        {
            return ((X2 - X1) * (pY - Y1) - (pX - X1) * (Y2 - Y1)) > 0.000000001f;
        }
    }
}
