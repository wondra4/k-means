﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace K_means_demo.Model
{
    public class Cluster : INotifyPropertyChanged
    {
        private List<DataPoint> _points;
        public List<DataPoint> Points
        {
            get
            {
                return _points;
            }
            set { _points = value; }
        }
        
        public VoronoiCell VoronoiCell { get; set; }

        private DataPoint _centroid;
        public DataPoint Centroid
        {
            get { return _centroid; }
            set
            {
                if(_centroid != value)
                {
                    OldCentroid = _centroid;
                    _centroid = value;
                    _centroid.Cluster = this;
                    OnPropertyChanged();
                }
            }
        }

        private DataPoint _oldCentroid;
        public DataPoint OldCentroid
        {
            get { return _centroid; }
            set
            {
                if (_oldCentroid != value)
                {
                    _oldCentroid = value;
                    OnPropertyChanged();
                }
            }
        }


        private Brush _brush;
        public Brush Brush
        {
            get { return _brush; }
            set { _brush = value; }
        }

        public Cluster()
        {
            _points = new List<DataPoint>();
            VoronoiCell = new VoronoiCell();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName]string name = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    

}
