﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace K_means_demo.Model
{
    //public class PointEqualityComparer : IEqualityComparer<Point>
    //{
    //    public bool Equals(Point a, Point b)
    //    {
    //        return a.X == b.X && a.Y == b.Y;
    //    }

    //    public int GetHashCode(Point other)
    //    {
    //        return other.X.GetHashCode() * 19 + other.Y.GetHashCode();
    //    }
    //}

    public class VoronoiCell : INotifyPropertyChanged
    {
        private List<CellEdge> _edges;
        public List<CellEdge> Edges
        {
            get { return _edges; }
            set { _edges = value; }
        }

        private PointCollection _polygon;
        public PointCollection Polygon
        {
            get { return _polygon; }
            set { _polygon = value; }
        }


        //public Polygon Polygon { get; set; }

        public Cluster Cluster { get; set; }

        public VoronoiCell()
        {
            _edges = new List<CellEdge>();
            _polygon = new PointCollection();
        }

        public void MakePolygon(Range rangeX, Range rangeY)
        {
            var points = new List<Point>();
            var ccw_edges = _edges.Select(e => e.isLeft(Cluster.Centroid.X, Cluster.Centroid.Y) ? e : new CellEdge() { X1 = e.X2, Y1 = e.Y2, X2 = e.X1, Y2 = e.Y1 });
            //add corners if neccessary
            if (!ccw_edges.Any(e => !e.isLeft(rangeX.From, rangeY.From))) //does (not) exist any edge that has corner by right side?
                points.Add(new Point(rangeX.From, rangeY.From));
            if (!ccw_edges.Any(e => !e.isLeft(rangeX.To, rangeY.From)))
                points.Add(new Point(rangeX.To, rangeY.From));
            if (!ccw_edges.Any(e => !e.isLeft(rangeX.From, rangeY.To)))
                points.Add(new Point(rangeX.From, rangeY.To));
            if (!ccw_edges.Any(e => !e.isLeft(rangeX.To, rangeY.To)))
                points.Add(new Point(rangeX.To, rangeY.To));

            //add unique points of cell
            points.AddRange(_edges.SelectMany(e => new List<Point>() { new Point(e.X1, e.Y1), new Point(e.X2, e.Y2) }).Distinct());
            //sort
            _polygon = new PointCollection(points.OrderByDescending(p => Math.Atan2(p.X - Cluster.Centroid.X, p.Y - Cluster.Centroid.Y)));

            OnPropertyChanged("Polygon");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName]string name = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

    }
}
