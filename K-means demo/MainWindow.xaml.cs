﻿using K_means_demo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace K_means_demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private DataSet _dataSet;

        public DataSet DataSet
        {
            get {
                return _dataSet ;
            }
            set {
                _dataSet = value;
            }
        }
        

        public MainWindow()
        {
            _dataSet = new DataSet();
            InitializeComponent();
            this.DataContext = this;
        }

        private DataPoint _draggedPoint = null;

        private void NotifyEllipse_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var n = sender as Controls.NotifyEllipse;
            if(n != null && DataSet.InteractionMode == InteractionCommandMode.moveVertex)
                _draggedPoint = n.DataContext as DataPoint;
        }
        

        private void TraingSetCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            _draggedPoint = null;
        }

        private void TraingSetCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if(_draggedPoint != null)
            {
                var mPos = Mouse.GetPosition(TraingSetCanvas);
                
                double sX = TraingSetCanvas.ActualWidth / (_dataSet.RangeX.To - _dataSet.RangeX.From);
                double sY = TraingSetCanvas.ActualHeight / (_dataSet.RangeY.To - _dataSet.RangeY.From);

                _draggedPoint.X = (mPos.X / sX) + _dataSet.RangeX.From;
                _draggedPoint.Y = (mPos.Y / sY) + _dataSet.RangeY.From;
            }
        }

        private void TraingSetCanvas_MouseLeave(object sender, MouseEventArgs e)
        {
            _draggedPoint = null;
        }
    }
}
